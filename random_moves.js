/*moves a certain percentage of each model's total images into the test directory
 * if test_percent is x%, then x% of each model (rounded down) will be represented
 * in test directory
 */

var fs = require('fs');
var path = require('path');
var map = {};
var index = [];
var args = process.argv.slice(2);
var model = args[0];
var test_percent = .2;
var train = path.dirname(fs.realpathSync(__filename)) + "/" + model + "_images_train/";
var test = path.dirname(fs.realpathSync(__filename)) + "/" + model + "_images_val/";
function createDirectory(path) {
	if(!fs.existsSync(path)){
		fs.mkdirSync(path, 0766, function(err){
			if(err){ 
				console.log(err);
				response.send("ERROR! Can't make the directory! \n");    
				// echo the result back
			}
		});   
	}
};

function move_files() {
	fs.readdir(train, function(err, files) {
		/*move test_percent of the total images into the test directory*/
		for (var i = 0; i < files.length; i++) {
			var params = files[i].split("_");
			if (map[params[1]] > 0) {
				map[params[1]]--;
				var oldPath = train + files[i];
				var newPath = test + files[i];
				fs.rename(oldPath, newPath, function(err) {
					if (err) {
						console.log(err);
					}
				});
			}
		}
	});
}
var all = fs.readdir(train, function(err, files) {
	/*repeated calls from bash script will not move more files to test directory*/
	if(fs.existsSync(test)) {
		return;
	}
	else {
		createDirectory(test);
	}
	/*count frequency of each model*/
	for (var i = 0; i < files.length; i++) {
		var params = files[i].split("_");
		console.log(params[1]);
		if (!map[params[1]]) {
			map[params[1]] = 1;
			index.push(params[1]);
		}
		else {
			map[params[1]]++;
		}
	}
	for (var i = 0; i < index.length; i++) {
		var test_size = map[index[i]] * test_percent;
		map[index[i]] = test_size;
	}
	move_files();
});