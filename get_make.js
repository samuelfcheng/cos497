var fs = require('fs');
var path = require('path');
var dirPath = path.dirname(fs.realpathSync(__filename)) + "/train";

var map = {};
var count = 0;
var car_set = new Set();
function get_prices() {
    car_set.forEach(function average(index) {
	console.log(index + " " + map[index]);
    });
}

var all = fs.readdir(dirPath, function(err, files) {
    if (!err) {
	for (var i = 0; i < files.length; i++) {
	    var params = files[i].split("_");
	    var price = parseInt(params[3]);
	    var make = params[0].toLowerCase();
	    var model = params[1].toLowerCase();
	    var year = params[2];
	    var index = make;
	    if (!map[index]) {
		map[index] = count;
		count++;
		car_set.add(index);
	    }
	}
    get_prices();
    }
    else {
	console.log(err);
    }
});

