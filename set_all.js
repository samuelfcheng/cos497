var fs = require('fs');
var path = require('path');
var args = process.argv.slice(2);
var model = args[0];
var dirPath = path.dirname(fs.realpathSync(__filename)) + "/" + model + "_labels.txt";
var train = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_train/";
var test = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_val/";
var current = train;
if (args[1]) {
	current = test;
}
var map = {};

function read_names(err, files) {
	for (var i = 0; i < files.length; i++) {
		var params = files[i].split("_");
//		console.log(params[0].toLowerCase());
		var index = params[0] + "_" + params[1];
	    index = index.toLowerCase();
		var label = map[index];
	    console.log(index);
		console.log(files[i] + " " + label);
		if (!label) {
			console.log("invalid label");
			break;
		}
	}

};




fs.readFile(dirPath, 'utf8', function (err,data) {
	if (err) {
		return console.log(err);
	}
	var lines = data.split("\n");
	for (var i = 0; i < lines.length; i++) {
		var params = lines[i].split(" ");
		if (params.length == 2) {
			map[params[0]] = params[1];
			var removed_dash = params[0].replace(/-/g, "");
			map[removed_dash] = params[1];
		    console.log(removed_dash);
		}
	}
	fs.readdir(current, read_names);
});
