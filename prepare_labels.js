var fs = require('fs');
var path = require('path');

var train = path.dirname(fs.realpathSync(__filename)) + "/train/";
var test = path.dirname(fs.realpathSync(__filename)) + "/test/";

var current = train;

function read_names(err, files) {
    for (var i = 0; i < files.length; i++) {
	var params = files[i].split("_");
	var label = params[2];
	console.log(files[i] + " " + label);
    }
};

fs.readdir(current, read_names);
