/*moves a certain percentage of each model's total images into the test directory
 * if test_percent is x%, then x% of each model (rounded down) will be represented
 * in test directory
 */

var fs = require('fs');
var path = require('path');
var map = {};
var index = [];
var args = process.argv.slice(2);
var model = args[0];
var test_percent = .2;
var train = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_train/";
var test = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_val/";
var flag = 0;
var indices = [];
var totals = {};
var frequency = {};

function compile(err, files) {
    for (var i = 0; i < files.length; i++) {
	if (files[i].length > 1) {
	    var params = files[i].split("_");
	    var make = params[0].toLowerCase();
	    var model = params[1].toLowerCase();
	    var index = make + model;
	    var year = params[2];
	    var price = parseInt(params[3]);
	    if (!totals[index]) {
		var years = {};
		var years_freq = {};
		totals[index] = years;
		frequency[index] = years_freq;
		indices.push(index);
	    }
	    var years = totals[index];
	    var years_freq = frequency[index];
	    if (!years[year]) {
		years[year] = price;
		years_freq[year] = 1;
	    }
	    else {
		years[year] += price;
		years_freq[year] += 1;
	    }  
	}
    }
    flag += 1;
    if (flag == 1) { 
	fs.readdir(test, compile);
    }
    else {
	console.log(indices.length);
	for (var i = 0; i < indices.length; i++) {
	    console.log(indices[i]);
	    var years = totals[indices[i]];
	    var years_freq = frequency[indices[i]];
	    var f = "";
	    for (var year in years) {
		if (years.hasOwnProperty(year)) {
		    var average = years[year] / years_freq[year];
		    var dist = .15 * average;
		    if (dist < 500) {
			dist = 500;
		    }
		    var min = Math.round(100 * (average - dist)) / 100;
		    var max = Math.round(100 * (average + dist)) / 100;
		    f += year + ": " + min + " - " + max + "\n";
		}
	    }
	    console.log(f);
	    fs.writeFile("prices/" + indices[i], f, function(err) {
		if (err) {
		    console.log(err);
		}
	    });
	}
    }
};
var all = fs.readdir(train, compile);
