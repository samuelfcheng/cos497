var fs = require('fs');
var path = require('path');
var map = {};

var dirPath = path.dirname(fs.realpathSync(__filename));

var file = dirPath + "/chev";
var train = dirPath + "/chevrolet_images_train/";
var test = dirPath + "/chevrolet_images_val/";



var get_map = fs.readFile(file, 'utf-8', function(err, data) {
//    map = data.split('\n');
    if (err) {
	console.log(err);
    }
    else {
	map = data.split('\n');
    }
});





var all = fs.readdir(train, function(err, files) {
    for (var i = 0; i < files.length / 50; i++) {
	var m = Math.floor(Math.random() * (files.length));
	if (files[m] != 0) {
	    var oldPath = train + files[m];
	    var newPath = test + files[m];
	    
	    fs.createReadStream(oldPath + files[m])
		.pipe(fs.createWriteStream(newPath + files[m]));			
	}
    }
});
