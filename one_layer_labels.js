/*moves a certain percentage of each model's total images into the test directory
 * if test_percent is x%, then x% of each model (rounded down) will be represented
 * in test directory
 */

var fs = require('fs');
var path = require('path');
var map = {};
var index = [];
var args = process.argv.slice(2);
var model = args[0];
var test_percent = .2;
var train = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_train/";
var test = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_val/";
var dirPath = path.dirname(fs.realpathSync(__filename)) + "/allmakes.txt";
var count = 0;
function populate_map(err, files) {
    for (var i = 0; i < files.length; i++) {
	if (files[i].length > 1) {
	var params = files[i].split("_");
	    var make = params[0];
	    var model = params[1];
	    var both = make + "_" + model;
	    if (!map[both]) {
		map[both] = 1;
		map[make.toLowerCase()] = 1;
//		console.log(both.toLowerCase() + " " + count);
		count++;
	    }
	}
    }
    fs.readFile(dirPath, "utf8", function(err, data) {
	if (err) {
	    console.log(err);
	}
	else {
	    var lines = data.split("\n");
	    for (var i = 0; i < lines.length; i++) {
		if (!map[lines[i]]) {
		    console.log(lines[i]);
		}
	    };
	}
    });
}
fs.readdir(train, populate_map);


