#!/bin/bash
cd ~/caffe
cd examples
mkdir $1_net
cp toyota_net/*.sh $1_net
cd $1_net
sed -i -e "s/toyota/$1/g" *.sh
cd ~/caffe/data
mkdir $1_net
cd $1_net
cp ~/caffe/cos497/labels/$1_train.txt .
cp ~/caffe/cos497/labels/$1_val.txt .
cd ~/caffe/models
mkdir $1_net
cp toyota_net/*.prototxt $1_net
cd $1_net
sed -i -e "s/toyota/$1/g" *.prototxt
cd ~/caffe
./examples/$1_net/create_imagenet.sh
./examples/$1_net/make_imagenet_mean.sh
./examples/$1_net/train_caffenet.sh
