var fs = require('fs');
var path = require('path');
var map = {};
var train = path.dirname(fs.realpathSync(__filename)) + "/train/";
var test = path.dirname(fs.realpathSync(__filename)) + "/test/";
var t = path.dirname(fs.realpathSync(__filename)) + "/t/";
var all = fs.readdir(train, function(err, files) {
    for (var i = 0; i < 10000; i++) {
	var m = Math.floor(Math.random() * (files.length));
	if (files[m] != 0) {
	    var oldPath = train + files[m];
	    var newPath = test + files[m];
	    fs.rename(oldPath, newPath, function(err) {
		if (err) {
		    console.log(err);
		}
		files[m] = 0;
	    });
	}
    }
});
