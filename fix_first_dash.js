/*moves a certain percentage of each model's total images into the test directory
 * if test_percent is x%, then x% of each model (rounded down) will be represented
 * in test directory
 */

var fs = require('fs');
var path = require('path');
var map = {};
var index = [];
var args = process.argv.slice(2);
var model = args[0];
var current = path.dirname(fs.realpathSync(__filename)) + "/images/" + model;

function move_files() {
	fs.readdir(train, function(err, files) {
		/*move test_percent of the total images into the test directory*/
		for (var i = 0; i < files.length; i++) {
		    if (files[i].length > 0) {
			var oldPath = current + files[i];
			var newName = files[i].replace("_", "-");
			var newPath = current + newName;
			fs.rename(oldPath, newPath, function(err) {
			    if (err) {
				console.log(err);
			    }
			});	
		    }
		}
	});
}

move_files();
