#!/bin/bash
node get_model_names.js $1 > $1
node get_model.js $1 > $1_labels.txt
node set_makes.js $1 > labels/$1_train.txt
node set_makes.js $1 test > labels/$1_val.txt
cd ~/caffe
cd data/$1_net
cp ~/caffe/cos497/labels/$1_train.txt .
cp ~/caffe/cos497/labels/$1_val.txt .
cd ~/caffe
./examples/$1_net/create_imagenet.sh
./build/tools/caffe test -model models/all_net/train_val.prototxt -weights models/all_net/all_train_iter_100545.caffemodel -gpu 0 -iterations 100
