var args = process.argv.slice(2);
var fs = require('fs');
var request = require("request"),
async = require("async"),
path = require('path'),
$ = require("cheerio"),
listings_url = 'https://www.truecar.com/used-cars-for-sale/listings/' + args[0];
var dirPath = path.dirname(fs.realpathSync(__filename)) + "/make_names/";
var makePath = dirPath + "makes.txt";
var buffer;
var currentMake;
var i = 0;
var dropdown_index = 12;

function get_model_names(error, response, body) {
	if (!error) {
		var all = $.load(body);
		var model_drop = all('.dropdown-menu').eq(12);
		model_drop.children().map(function(index, current) {
		    var model = $(current)
			.text()
			.trim()
			.replace(/ /g, '-')
			.toLowerCase();
			console.log(model);
		});
//		fs.writeFile(dirPath + (i++), buffer.toString(), function(err) {
//			if(err) {
//				return console.log(err);
//			}
//
//			console.log("The file was saved!");
//		}); 
	}
	else {
		console.log("We have encountered an error: " + error);
	}
}


request(listings_url, get_model_names);
//
//fs.readFile(makePath, 'utf-8', function (err,data) {
//	if (err) {
//		return console.log(err);
//	}
//	var lines = data.split("\n");
//	for (var i = 0; i < lines.length; i++) {
//		var extension = lines[i] + "/";
//		request(listings_url + extension, get_model_names);
//	}
//});
