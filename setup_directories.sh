#!/bin/bash
node get_model_names.js $1 > $1
node get_model.js $1 > $1_labels.txt
cd images
mv $1_images $1_images_train
cd ..
node move.js $1
node set_models.js $1 > labels/$1_train.txt
node set_models.js $1 test > labels/$1_val.txt
cd ~/caffe
cd examples
mkdir $1_net
cp jeep_net/*.sh $1_net
cd $1_net
sed -i -e "s/jeep/$1/g" *.sh
cd ~/caffe/data
mkdir $1_net
cd $1_net
cp ~/caffe/cos497/labels/$1_train.txt .
cp ~/caffe/cos497/labels/$1_val.txt .
cd ~/caffe/models
mkdir $1_net
cp jeep_net/*.prototxt $1_net
cd $1_net
sed -i -e "s/jeep/$1/g" *.prototxt
cd ~/caffe/cos497
node append_filler.js $1 > synsets/$1_synset.txt
cd ~/caffe
./examples/$1_net/create_imagenet.sh
./examples/$1_net/make_imagenet_mean.sh
./build/tools/caffe train -solver models/$1_net/solver.prototxt -weights models/all_net/all_train_iter_100545.caffemodel -gpu 0
