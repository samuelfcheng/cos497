var fs = require('fs');
var request = require("request"),
async = require("async"),
path = require('path'),
$ = require("cheerio");
var page_number = 1;
var start = 1;
var c = 0;
var current_file = "hyundai";
var listings_url = 'https://www.truecar.com/used-cars-for-sale/listings/';
var terminate = '/?page=';
var model_index = 0;
var terminal = 'but we found these similar listings for';
var end = 0;
var debug_mode = 0;
/*store any ids of intervals set*/
var timeouts = [];
/*array of all models to parse*/
var m = [];
/*set of url to prevent repeat visits*/
var url_set = {};

/*download file into local directory*/
var download = function(uri, filename, callback){
    request.head(uri, function(err, res, body){
	request(uri)
	    .pipe(fs.createWriteStream(filename))
	    .on('close', callback);
    });
};

/*clear all timeouts*/
function clear_timeouts() {
    for (var j = 0; j < timeouts.length; j++) {
	clearTimeout(timeouts[j]);
    }
    timeouts = [];
}

/*count images visited*/
var count = function() {
	c++;
	if (c % 100 == 0 && debug_mode != 0) {
		console.log(c);
	}
};

function createDirectory(path) {
    if(!fs.existsSync(path)){
	if (debug_mode != 0) {
	    console.log("attempt to create dir");
	}
	fs.mkdirSync(path, 0766, function(err){
	    if (debug_mode != 0) {
		console.log("successfully created dir");
	    }
	    if(err){ 
		console.log(err);
		response.send("ERROR! Can't make the directory! \n");    
		// echo the result back
	    }
	});   
    }
    else {
	if (debug_mode != 0) {
	    console.log("already exists");
	}
    }
};


var dirPath = path.dirname(fs.realpathSync(__filename)) + "/" + current_file
    + "_images";
createDirectory(dirPath);


function explore(page) {
    var i = 0;
    var listing_str = "/used-cars-for-sale/listing/";
    console.log(page.indexOf(terminal));
    if (page.indexOf(terminal) > 0) {
	clear_timeouts();
	model_index++;
	start = 1;
	if (timeouts.length == 0) {
	    timeouts.push(setInterval(grab_pages, 800));
	}
	return;
    }
    var index = page.indexOf(listing_str, i);
    i = index + listing_str.length;
    var n = 0;
    while (index > 0) {
	var close = page.indexOf("\"", index);
	if (close < 0) {
	    throw "bad string search";
	}
	var url = "https://www.truecar.com" + page.substring(index, close);
	n++;
	if (!url_set[url]) {
			url_set[url] = 1;
	    request(url, download_carousel);
	}
	else {
	    if (debug_mode != 0) {
		console.log("SKIPPING");
		console.log(url);
	    }
	}
	i = index + listing_str.length;
	index = page.indexOf(listing_str, i);
    }
}

function randomInt() {
    return Math.floor(Math.random() * (99999999));
}

function download_carousel (error, response, body) {
    if (!error) {
	var all = $.load(body);
	var price = all('.price').find('.h2').text().replace(/[$,]+/g,"");
	var round = Math.floor((parseInt(price) + 499) / 500) * 500;
	all('img').map(function(i, link) {
	    var jpg = $(link).attr('src');
	    if (jpg) {
		if (jpg.indexOf('vehicle-images/inventory') > -1) {
		    var info = $(link).attr('alt');
		    var first_space = info.indexOf(' ');
		    var year = info.substring(0, first_space);
		    var make_and_model = info.substring(first_space + 1,
							info.length);
		    var next_space = make_and_model.indexOf(' ');
		    var make = make_and_model.substring(0, next_space)
			.replace(" ", "");
		    var model = make_and_model
			.substring(next_space + 1,
				   make_and_model.length)
			.replace(/\W+/g, "");
		    var name = make + "_" + model + "_" + year 
			+ "_" + round + "_" + randomInt() + ".jpg";
		    if (debug_mode != 0) {
			console.log(name);
		    }
		    download(jpg,dirPath + "/" + name, count);
		};
	    };
	});
    }
    else {
	console.log("We have encountered an error: " + error);
    }
}

function download_images (error, response, body) {
    if (!error) {
	var all = $.load(body);
	explore(all.html());
    }
    else {
	console.log("We have encountered an error: " + error);
    }
};

function grab_pages() {
    if (model_index > m.length || !m[model_index]) {
	clear_timeouts();
	return;
    }
    var make_model_url = listings_url + current_file + "/" 
	+ m[model_index].toLowerCase() + terminate;
    for (var i = 0; i < 1; i++) {
	var current_url = make_model_url + start;
	if (debug_mode != 0) {
	    console.log(current_url);
	}
	console.log(current_url + " " + i + " " + end);
	request(current_url, download_images);
	start++;
    }
}

fs.readFile(current_file, 'utf8', 
	    function(err, data) {
		if (err) {
		    console.log(err);
		}
		m = data.split('\n');
		if (debug_mode != 0) {
		    console.log(m);
		}	    
	    });

timeouts.push(setInterval(grab_pages, 800));
