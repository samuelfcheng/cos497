/*moves a certain percentage of each model's total images into the test directory
 * if test_percent is x%, then x% of each model (rounded down) will be represented
 * in test directory
 */

var fs = require('fs');
var path = require('path');
var map = {};
var index = [];
var args = process.argv.slice(2);
var model = args[0];
var test_percent = .2;
var train = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_train/";
var test = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_val/";

var map = {};

function move_files() {
    fs.readdir(test, function(err, files) {
	for (var i = 0; i < files.length; i++) {
	    if (files[i].length > 1) {
		var params = files[i].split("_");
		if (map[params[1]]) {
		    map[params[1]]++;
		}
		else {
		    map[params[1]] = 1;
		}	
	    }
	}
	for (var property in map) {
	    if (map.hasOwnProperty(property)) {
		console.log(property + " " + map[property]);
	    }
	}
    });
}
move_files();
