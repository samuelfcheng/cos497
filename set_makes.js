var fs = require('fs');
var path = require('path');


var args = process.argv.slice(2);
var model = args[0];

var dirPath = path.dirname(fs.realpathSync(__filename)) + "/all_labels.txt";
var train = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_train/";
var test = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_val/";
var current = train;
var map = {};
if (args[1]) {
    current = test;
}

function read_names(err, files) {
	for (var i = 0; i < files.length; i++) {
	    var params = files[i].split("_");
//	    console.log(params[0].toLowerCase());
	    var label = map[params[0].toLowerCase()];
		console.log(files[i] + " " + label);
	}
};




fs.readFile(dirPath, 'utf8', function (err,data) {
    if (err) {
	return console.log(err);
    }
    var lines = data.split("\n");
    for (var i = 0; i < lines.length; i++) {
	var params = lines[i].split(" ");
	if (params.length == 2) {
	    map[params[0]] = params[1];
	}
    }
    fs.readdir(current, read_names);
});
