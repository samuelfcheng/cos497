var fs = require('fs');
var request = require("request"),
async = require("async"),
path = require('path'),
$ = require("cheerio"),
listings_url = 'https://www.truecar.com/used-cars-for-sale/listings/?page=';
var page_number = 1;
var start = 1;
var c = 0;
var url_set = new Set();
var download = function(uri, filename, callback){
	request.head(uri, function(err, res, body){
	    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
	});
};

var count = function() {
    c++;
    if (c % 100 == 0) {
	console.log(c);
    }
};

var dirPath = path.dirname(fs.realpathSync(__filename)) + "/images";

function explore(page) {
    var i = 0;
    var listing_str = "/used-cars-for-sale/listing/";
    var index = page.indexOf(listing_str, i);
    i = index + listing_str.length;
    var n = 0;
    while (index > 0) {
	var close = page.indexOf("\"", index);
	if (close < 0) {
	    throw "bad string search";
	}
	var url = "https://www.truecar.com" + page.substring(index, close);
	n++;
	if (!url_set.has(url)) {
	    url_set.add(url);
	    request(url, download_carousel);
	}
	i = index + listing_str.length;
	index = page.indexOf(listing_str, i);
    }
    console.log(n >= 15);
}

function randomInt() {
    return Math.floor(Math.random() * (9999999));
}

function download_carousel (error, response, body) {
    start = start + 1;
    if (!error) {
	var all = $.load(body);
	var price = all('.price').find('.h2').text().replace(/[$,]+/g,"");
	var round = Math.floor((parseInt(price) + 499) / 500) * 500;
	//		console.log(price + " " + price.length);
	//		each(function(i, element) {
	//		var a = $(this);
	//		console.log(a.text());
	//		});
	all('img').map(function(i, link) {
	    var jpg = $(link).attr('src');
	    if (jpg) {
		if (jpg.indexOf('vehicle-images/inventory') > -1) {
		    //					console.log(jpg);
					var info = $(link).attr('alt');
		    var first_space = info.indexOf(' ');
		    var year = info.substring(0, first_space);
		    var make_and_model = info.substring(first_space + 1,
							info.length);
		    var next_space = make_and_model.indexOf(' ');
		    var make = make_and_model.substring(0, next_space).replace(" ", "");
		    var model = make_and_model.substring(next_space + 1,
							 make_and_model.length).replace(/\W+/g, "");
		    
		    //					var makePath = dirPath + "/" + make;
//					var modelPath = makePath + "" +
//					"/" + model;
		    //					var yearPath = modelPath + "/" + year;
////					createDirectory(makePath);
////					createDirectory(modelPath);
////					createDirectory(yearPath);
//					console.log(jpg);
		    var name = make + "_" + model + "_" + year + "_" + round + "_" + randomInt() + ".jpg";
		    console.log(name);
		    download(jpg,dirPath + "/" + name, count);
		};
	    };
	});
    }
    else {
	console.log("We have encountered an error: " + error);
    }
}

function download_images (error, response, body) {
    if (!error) {
	var all = $.load(body);
	explore(all.html());
	//		all('img').map(function(i, link) {
	//		var jpg = $(link).attr('src');
//		if (jpg) {
//		if (jpg.indexOf('vehicle-images/inventory') > -1) {
//		var info = $(link).attr('alt');
//		var first_space = info.indexOf(' ');
//		var year = info.substring(0, first_space);
//		var make_and_model = info.substring(first_space + 1,
//		info.length);
//		var next_space = make_and_model.indexOf(' ');
//		var make = make_and_model.substring(0, next_space);
//		var model = make_and_model.substring(next_space + 1,
//		make_and_model.length);
//		var makePath = dirPath + "\\" + make;
//		var modelPath = makePath + "\\" + model;
//		var yearPath = modelPath + "\\" + year;
//		createDirectory(makePath);
//		createDirectory(modelPath);
//		createDirectory(yearPath);
//		console.log(jpg);
//		download(jpg, yearPath + "\\" + i + ".jpg", count);
//		};
//		};
//		});
		/*console.log($('.row', 'col-xs-6 col-sm-4 col-md-4',
	  '.vdp-link').attr('img'));*/
	}
    else {
	console.log("We have encountered an error: " + error);
    }
};
function scrape() {
    request(listings_url + page_number, download_images);
    page_number = page_number + 1;
}
var last = 0;
function grab_pages() {
    if (last - c == 0) {
	start++;
    }
    last = c;
    if (start > 12000) {
	start = 0;
    }
    for (var i = 0; i < 10; i++) {
	request(listings_url + start, download_images);
	start++;
	console.log(start);
    }
}
//request(listings_url + start, download_images);
setInterval(grab_pages, 4000);
//var a = [];
//for (var i = 1; i < 10000; i++) {
//a.push(grab_ten_pages);
//}
//console.log(a.length);
//async.series(a, 
//function(err, results) {
//console.log(err);
//});

function createDirectory(path) {
    if(!fs.existsSync(path)){
	//		console.log("attempt to create dir");
	fs.mkdirSync(path, 0766, function(err){
	    //			console.log("successfully created dir");
	    if(err){ 
		console.log(err);
		response.send("ERROR! Can't make the directory! \n");    // echo the result back
	    }
	});   
    }
    else {
	//		console.log("already exists");
    }
};
