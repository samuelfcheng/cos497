var fs = require('fs');
var path = require('path');

var dirPath = path.dirname(fs.realpathSync(__filename)) + "/prices.txt";
var train = path.dirname(fs.realpathSync(__filename)) + "/train/";
var test = path.dirname(fs.realpathSync(__filename)) + "/test/";
var current = train;
var map = {};

function read_names(err, files) {
	for (var i = 0; i < files.length; i++) {
		var params = files[i].split("_");
		var index = params[0] + "_" + params[1] + "_" + params[2];
		var price = parseFloat(map[index.toLowerCase()]);
		var bucket = Math.floor((price + 999) / 1000) * 1000;
		var label = bucket / 1000;
		console.log(files[i] + " " + (label - 1) + ", " + label + ", " + (label + 1));
	}
};


fs.readFile(dirPath, 'utf8', function (err,data) {
	if (err) {
		return console.log(err);
	}
	var lines = data.split("\n");
	for (var i = 0; i < lines.length; i++) {
		var params = lines[i].split(" ");
		if (params.length == 2) {
			map[params[0]] = params[1];
		}
	}
	fs.readdir(current, read_names);
});
