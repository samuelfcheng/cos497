var fs = require('fs');
var path = require('path');
var args = process.argv.slice(2);
var model = args[0];
var dirPath = path.dirname(fs.realpathSync(__filename)) + "/" + model + "_labels.txt";
var train = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_train/";
var test = path.dirname(fs.realpathSync(__filename)) + "/images/" + model + "_images_val/";
var current = train;
if (args[1]) {
	current = test;
}

function read_names(err, files) {
    for (var i = 0; i < files.length; i++) {
	var params = files[i].split("_");
	var label = parseInt(params[3]) - 1;
	if (label < 2000) {
	    label = 0;
	}
	else if (label < 4000) {
	    label = 1;
	}
	else {
	    label = Math.floor(label / 4000) + 1;
	}
	console.log(files[i] + " " + label);
	if (!label && label != 0) {
	    console.log("invalid label");
	    break;
	}
    }
    
};

fs.readdir(current, read_names);
